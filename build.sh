# Environment Setup
mkdir $HOME/.dwm $HOME/.git $HOME/.config 
cp -rfv packages/local/autostart.sh $HOME/.dwm/autostart.sh
cp -rfv  packages/local/dwm-bar $HOME/.git/
cp -rfv packages/local/wallpapers $HOME/.git/
cp -rfv packages/local/alacritty $HOME/.config/  
cp -rfv packages/local/picom $HOME/.config/
cp -rfv packages/local/.xinitrc $HOME/
cp -rfv packages/local/.bash_profile $HOME
# Attemp to Install a required dependencies 
# dwm-bar deps 
sudo pacman -S $(echo $(cat packages/local/dwm-bar/dep/arch.txt | cut -d' ' -f1))

# Base deps
declare -a deps=("lxqt-policykit-git" "alacritty-git" "pulseaudio-git" "ttf-font-awesome" "pamixer-git" "nerd-fonts-complete" "rofi-git" "htop-temperature" "dmenu2")

for dependencies in ${deps[@]}; do
	yay -S  $dependencies
done

sudo make clean install 
