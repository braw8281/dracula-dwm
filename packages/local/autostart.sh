picom --config $HOME/.config/picom/picom.conf &
nitrogen --set-scaled $HOME/.git/wallpapers/dracula-spooky-44475a.png &
pulseaudio -D &
$HOME/.git/dwm-bar/dwm_bar.sh &
lxqt-policykit-agent &
